extends StaticBody2D

export var speed = 5

func _ready():
	constant_linear_velocity.x = speed
	$AnimationPlayer.playback_speed = speed/5
	$AnimationPlayer.play("run")
	print(name + " : " + str($AnimationPlayer.playback_speed))
	print($AnimationPlayer)
