extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	var belts = get_tree().get_nodes_in_group('belt')
	for belt in belts:
		belt.play()
	
	$Pipe1/AnimatedPipe.play("default")
	$Pipe2/AnimatedPipe.play("default")
	$Pipe3/AnimatedPipe.play("default")
