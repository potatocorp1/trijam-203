extends Node2D

func _ready():
	turnOn()

func turnOn():
	$Diodgreen.visible = true
	$Diodred.visible = false
	$AnimationPlayer.stop()
	$Halo.visible = false
	$Halo2.visible = false

func turnOff():
	$Diodgreen.visible = false
	$Diodred.visible = true
	$AnimationPlayer.play("alarm")
	$Halo.visible = true
	$Halo2.visible = true
