extends "res://Machine.gd"

func _ready():
	pass # Replace with function body.

func break_machine():
	.break_machine()
	$MachineSprite.frame=0
	$Sparkle.emitting = true

func repare_machine():
	.repare_machine()
	$Sparkle.emitting = false
