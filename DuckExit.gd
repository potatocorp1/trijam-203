extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.frame = 0


func update_sprite(produced, required):
	var first_stage = required/4
	var second_stage = 3*(required/4)
	if  produced < first_stage:
		$Sprite.frame = 0
	elif produced >= first_stage && produced < second_stage:
		$Sprite.frame = 1
	else:
		$Sprite.frame = 2
