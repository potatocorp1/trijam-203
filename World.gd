extends Node2D

const Stage1 = preload("res://stages/stage01.tscn")
var stage

func play():
	var stage = Stage1.instance()
	stage.connect("stage_completed", self, "_on_stage_completed")
	add_child(stage)

func _on_stage_completed(state):
	pass
