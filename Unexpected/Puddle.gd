extends Node2D

signal entered
signal exited


func _on_Area2D_body_exited(body):
	if body.name == "Player": 
		emit_signal("exited")

func _on_In_body_entered(body):
	if body.name == "Player": 
		emit_signal("entered")
