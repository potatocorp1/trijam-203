extends Node2D

const TIME_MAX = 60
const DUCK_REQUIRED = 30
const PLAYER_INIT_POS = Vector2(20, 135)
var nb_machines
var time_left
var duck_produced
var machine_contact = null
var nb_machines_broke = 0
const duck_scene = preload("res://ProductDuck.tscn")
const plastic_scene = preload("res://ProductBottle.tscn")

onready var menu = $Menu

signal stage_completed

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	time_left = TIME_MAX
	duck_produced = 0
	nb_machines = get_tree().get_nodes_in_group("machines").size()
	menu.setGoalExpl("Beware of puddles")

func game_over():
	$CycleTimer.stop()
	$DuckTimer.stop()
	get_tree().call_group("factory", "hide")
	get_tree().call_group("new_ducks", "queue_free")
	get_tree().call_group("new_plastics", "queue_free")
	get_tree().call_group("machines", "reset")
	$Player.hide()
	if duck_produced >= DUCK_REQUIRED:
		emit_signal("stage_completed", "victory")
	else:
		emit_signal("stage_completed", "defeat")

func new_game():
	time_left = TIME_MAX
	duck_produced = 0
	nb_machines_broke = 0
	
	get_tree().call_group("factory", "show")
	$Player.position = PLAYER_INIT_POS
	$Player.show()

	get_tree().call_group("machines", "reset")
	$HUD.update_timer(time_left)
	$HUD.update_score(duck_produced, DUCK_REQUIRED)

	$CycleTimer.start()
	$DuckTimer.start()

	$bed.play()

func break_machine():
	var machines = get_tree().get_nodes_in_group("machines")
	var selected_machine = randi()%nb_machines
	var tested_machines = 0

	while (!machines[selected_machine].is_running && tested_machines < nb_machines):
		tested_machines += 1
		selected_machine = (selected_machine + 1) % nb_machines

	if tested_machines < nb_machines:
		machines[selected_machine].break_machine()

func _on_CycleTimer_timeout():
	time_left -= 1
	if time_left == 0:
		game_over()
		
	if time_left >= 30:
		if time_left%8 == 0:
			break_machine()
	else:
		if time_left%4 == 0:
			break_machine()
	
	$HUD.update_timer(time_left)


func _on_DuckTimer_timeout():
	var duck = duck_scene.instance()
	duck.position = $DuckStartPosition.position
	duck.add_to_group("new_ducks")
	add_child(duck)
	
	var plastic = plastic_scene.instance()
	plastic.position = $PlasticStartPosition.position
	plastic.add_to_group("new_plastics")
	add_child(plastic)

func _on_DuckExit_body_entered(body):
	if body.is_in_group("new_ducks"):
		duck_produced += 1
		$HUD.update_score(duck_produced, DUCK_REQUIRED)
		body.queue_free()
		if duck_produced == DUCK_REQUIRED:
			emit_signal("stage_completed", "victory")

func _on_Player_machine_repared():
	$Player.set("can_repair", false)
	machine_contact.repare_machine()

func _on_machine_contact(machine):
	machine_contact = machine
	$Player.set("can_repair", true)

func _on_machine_away():
	machine_contact = null
	$Player.set("can_repair", false)

func _on_machine_broke():
	nb_machines_broke += 1
	$DuckTimer.wait_time = nb_machines_broke + 1
	if nb_machines_broke == nb_machines:
		$DuckTimer.stop()
		get_tree().call_group("pipes_anim", "hide")

func _on_machine_repared():
	nb_machines_broke -= 1
	$DuckTimer.wait_time = nb_machines_broke + 1
	if $DuckTimer.is_stopped():
		$DuckTimer.start()
		get_tree().call_group("pipes_anim", "show")

func _on_Menu_start_game():
	new_game()

func _on_Puddle_entered():
	$Player.slipOn()

func _on_Puddle_exited():
	$Player.slipOff()
