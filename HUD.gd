extends CanvasLayer

signal start_game

func update_score(score, goal):
	$ScoreLabel.text = str(score).pad_zeros(2)+"/"+str(goal)

func update_timer(time_left):
	var seconds = time_left%60
	var minute = (time_left - seconds)/60
	$TimerLabel.text = str(minute).pad_zeros(2)+":"+str(seconds).pad_zeros(2)+" s"
