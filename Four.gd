extends "res://Machine.gd"

func break_machine():
	.break_machine()
	$AnimationPlayer.stop()
	$fire.color = Color("2d2b2b")
	$flame.hide()
	$Smoke.emitting = false
	$DarkSmoke.emitting = true
	$DarkSmoke2.emitting = true
func repare_machine():
	.repare_machine()
	$AnimationPlayer.play("fire")
	$Smoke.emitting = true
	$flame.show()
	$DarkSmoke.emitting = false
	$DarkSmoke2.emitting = false
