extends CanvasLayer

signal start_game

func _ready():
	var clipboard = get_node("ClipboardLayout")
	for child in get_children():
		if !child.is_in_group("default_menu"):
			remove_child(child)
			if child.name == "StartButton":
				child.connect("pressed", self, "_on_StartButton_pressed")
			clipboard.add_child(child)

func setGoalExpl(goalExplication):
	$ClipboardLayout/Message.text = goalExplication

func end_game():
	$ClipboardLayout/Clipboard.show()

func show_game_over():
	end_game()

	$ClipboardLayout/Fired.show()
	$MessageTimer.start()
	yield($MessageTimer, "timeout")
	$ClipboardLayout/Fired.hide()

	$ClipboardLayout/Message.show()
	
	# Make a one-shot timer and wait for it to finish.
	yield(get_tree().create_timer(1), "timeout")
	$ClipboardLayout/StartButton.show()

func show_victory():
	end_game()

	$ClipboardLayout/Promoted.show()
	$MessageTimer.start()
	yield($MessageTimer, "timeout")
	$ClipboardLayout/Promoted.hide()

	$ClipboardLayout/Message.show()
	
	# Make a one-shot timer and wait for it to finish.
	yield(get_tree().create_timer(1), "timeout")
	$ClipboardLayout/StartButton.show()

func _on_StartButton_pressed():
	$ClipboardLayout/StartButton.hide()
	$ClipboardLayout/Clipboard.hide()
	$ClipboardLayout/Message.hide()
	emit_signal("start_game")

func update_score(score, goal):
	$ScoreLabel.text = str(score).pad_zeros(2)+"/"+str(goal)

func update_timer(time_left):
	$TimerLabel.text = "00:"+str(time_left)+" s"

func _on_MessageTimer_timeout():
	$ClipboardLayout/Message.hide()
