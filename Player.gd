extends KinematicBody2D

export var speed = 10
export var maxSpeed = 70

var slippery = 1
var screen_size
var velocity = Vector2()
var input_vector
var machine_accessible
var is_repairing = false
var can_repair = false
signal machine_repared


# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	machine_accessible = null

func get_input():
	input_vector = Vector2.ZERO
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x -= 1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
	if Input.is_action_pressed("ui_up"):
		input_vector.y -= 1
	
	if Input.is_action_just_pressed("ui_repair") && can_repair:
		is_repairing = true
		$RepairTimer.start()
		$AnimatedSprite.play("repair")
		$AudioStreamPlayer.play()
	if Input.is_action_just_released("ui_repair"):
		is_repairing = false
		$RepairTimer.stop()
		$AnimatedSprite.animation = "default"
		$AudioStreamPlayer.stop()

func _physics_process(delta):
	get_input()

	if is_repairing:
		return

	if input_vector != Vector2.ZERO:
		velocity += input_vector * speed
		velocity = velocity.clamped(maxSpeed)
		velocity.normalized()
	else:
		velocity = velocity.linear_interpolate(Vector2.ZERO, slippery)

	velocity = move_and_slide(velocity)

	if input_vector.x != 0 || input_vector.y != 0:
		$AnimatedSprite.animation = "run"
		$AnimatedSprite.flip_h = velocity.x < 0
	else:
		$AnimatedSprite.animation = "default"
	
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)

func _on_RepairTimer_timeout():
	is_repairing = false
	$AnimatedSprite.animation = "default"
	$AudioStreamPlayer.stop()
	emit_signal("machine_repared")

func slipOn():
	slippery = 0.02

func slipOff():
	slippery = 1
