extends "res://Machine.gd"

func break_machine():
	.break_machine()
	$OilLeak.visible = true

func repare_machine():
	.repare_machine()
	$OilLeak.visible = false
