extends StaticBody2D

export var repare_time = 2
var is_running = true
var has_player = false

signal machine_broke
signal machine_repared
signal machine_contact
signal machine_away

func break_machine():
	is_running = false
	if has_player:
		$EKey.visible = true
	$MachineSprite.stop()
	$Diode.turnOff()
	emit_signal("machine_broke")

func repare_machine():
	reset()
	emit_signal("machine_repared")
	
func reset():
	is_running = true
	$MachineSprite.play("working")
	$Diode.turnOn()
	$EKey.visible = false

func _on_InteractZone_body_entered(body):
	if body.name == "Player":
		has_player = true
		emit_signal("machine_contact", self)
		if !is_running:
			$EKey.visible = true

func _on_InteractZone_body_exited(body):
	if body.name == "Player":
		has_player = false
		emit_signal("machine_away")
		if !is_running:
			$EKey.visible = false
